//
//  TableViewController.h
//  BurguerMenu
//
//  Created by Jose Catala on 12/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewController : UITableViewController

@end
