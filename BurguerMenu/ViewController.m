//
//  ViewController.m
//  BurguerMenu
//
//  Created by Jose Catala on 12/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "ViewController.h"
#import "MenuTableViewController.h"

@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UIView *v = [self.view viewWithTag:666];
    
    if (v)
    {
        [self showBurgherMenu:NO view:v button:nil];
    }
}

#pragma mark BURGUER MENU STUFF

- (IBAction)btnBurguerMenuDidTap:(id)sender
{
    [(UIBarButtonItem *)sender setEnabled:NO];
    
    UIView *v = [self.view viewWithTag:666];
    
    if (!v)
    {
        [self createBurguerMenu];
        
        v = [self.view viewWithTag:666];
    }

    [self showBurgherMenu:v.frame.origin.x == 0 ? NO : YES view:v button:(UIBarButtonItem *)sender];
}

- (void) showBurgherMenu:(BOOL)show view:(UIView *)view button:(UIBarButtonItem *)button
{
    float x = show ? 0 : -self.view.frame.size.width;
    
    [UIView animateWithDuration:0.3 animations:^{
        view.frame = CGRectMake(x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
    }];
    
    if(button) button.enabled = YES;
}

- (void) createBurguerMenu
{
    MenuTableViewController *tbc = [[MenuTableViewController alloc]init];
    
    [self addChildViewController:tbc];
    
    tbc.view.tag = 666;
    
    [self.view addSubview:tbc.view];
    
    float x = -self.view.frame.size.width;
    float y = self.navigationController.navigationBar.frame.size.height + 20;
    float w = self.view.frame.size.width - 80;
    float h = self.view.frame.size.height - y;
    
    tbc.view.frame = CGRectMake(x, y, w, h);
    
    [tbc didMoveToParentViewController:self];
    
    tbc = nil;
}


@end
